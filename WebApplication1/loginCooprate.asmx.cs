﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;


namespace WebApplication1
{
    /// <summary>
    /// Summary description for loginCooprate
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class loginCooprate : System.Web.Services.WebService
    {

        SqlConnection sqlCon = null;

        public SqlConnection getConnection()
        {

            try
            {
                sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                sqlCon.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error connecting db " + ex);
            }
            return sqlCon;
        }

        [WebMethod]
        public string AutoCoopClientId()
        {
            string CoopClientId = null;
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("Select ClientId from CooperateClient", sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                string id = "";
                bool records = dr.HasRows;
                if (records)
                {
                    while (dr.Read())
                    {
                        id = dr[0].ToString();
                    }
                    string idString = id.Substring(1);

                    int CLR = Int32.Parse(idString);
                    if (CLR >= 1 && CLR < 9)
                    {
                        CLR = CLR + 1;
                        CoopClientId = "L00" + CLR;
                    }
                    else if (CLR >= 9 && CLR < 99)
                    {
                        CLR = CLR + 1;
                        CoopClientId = "L0" + CLR;
                    }
                    else if (CLR > 99)
                    {
                        CLR = CLR + 1;
                        CoopClientId = "L" + CLR;
                    }

                }
                else
                {
                    CoopClientId = "L001";
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                CoopClientId = ex.ToString();
            }
            return CoopClientId;
        }

        [WebMethod]
        public string insertClient(string ClientId, string CustomerName, string Address, string Email,string Package, string Password, string ConPassword)
        {
            int noRecords = 0;
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("insert into CooperateClient values('" + ClientId + "','" + CustomerName + "','" + Address + "','" + Email + "','" + Package + "','" + Password + "','" + ConPassword + "');", sqlCon);
                noRecords = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return noRecords.ToString();
        }

        [WebMethod]
        public bool authenticateClient(string Email, string Password)
        {
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("Select * from CooperateClient WHERE Email='" + Email + "'AND Password='" + Password + "'", sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                bool result = dr.HasRows;
                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
