﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        SqlConnection sqlCon = null;

        public SqlConnection getConnection()
        {

            try
            {
                sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                sqlCon.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error connecting db " + ex);
            }
            return sqlCon;
        }

        [WebMethod]
        public string AutoCourierId()
        {
            string PackageId = null;
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("Select PackageId from Courier1", sqlCon);
                SqlDataReader dr = cmd.ExecuteReader();
                string id = "";
                bool records = dr.HasRows;
                if (records)
                {
                    while (dr.Read())
                    {
                        id = dr[0].ToString();
                    }
                    string idString = id.Substring(1);

                    int STR = Int32.Parse(idString);
                    if (STR >= 1 && STR < 9)
                    {
                        STR = STR + 1;
                        PackageId = "C00" + STR;
                    }
                    else if (STR >= 9 && STR < 99)
                    {
                        STR = STR + 1;
                        PackageId = "C0" + STR;
                    }
                    else if (STR > 99)
                    {
                        STR = STR + 1;
                        PackageId = "C" + STR;
                    }

                }
                else
                {
                    PackageId = "C001";
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                PackageId = ex.ToString();
            }
            return PackageId;
        }

        [WebMethod]
        public string insertCourier(string PackageId, string Customer_Name, string Address, string Contact_Number, string Quantity, string NatureOfPackage, DateTime Date, String Price,String Tracking)
        {
            int NoRows = 0;
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("insert into Courier1 Values('" + PackageId + "','" + Customer_Name + "','" + Address + "','" + Contact_Number + "','" + Quantity + "','" + NatureOfPackage + "','" + Date + "','" + Price + "','"+ Tracking+"');", sqlCon);

                NoRows = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return NoRows.ToString();
        }

        [WebMethod]
        public DataSet SearchProduct(string PackageId)
        {
            DataSet ds = new DataSet();
            try
            {
                getConnection();
                SqlCommand cmd = new SqlCommand("Select * all from Courier1 where PackageId='" + PackageId + "'", sqlCon);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(ds, "Courier1");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Searching Package" + ex);
            }
            return ds;
        }
    }
}