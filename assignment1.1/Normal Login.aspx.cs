﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1._1
{
    public partial class Normal_Login : System.Web.UI.Page
    {
        ServiceReference2.loginSoapClient obj = new ServiceReference2.loginSoapClient();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (obj.authenticateNoClient(txtEmail.Text, txtPassword.Text))
            {
                Response.Redirect("WebForm1.aspx");
            }

            else
            {
                Response.Write("<script>alert('Invalid username or password')</script>");
            }
        }

        protected void btnRegisterP_Click(object sender, EventArgs e)
        {
            Response.Redirect("Normal Register.aspx");
        }
    }
}