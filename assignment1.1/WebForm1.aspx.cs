﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace assignment1._1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        ServiceReference1.WebService1SoapClient obj = new ServiceReference1.WebService1SoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtPackageId.Text = obj.AutoCourierId();

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string value = obj.insertCourier(txtPackageId.Text, txtCusName.Text, txtAddress.Text, txtConNumber.Text, txtQuantity.Text, txtNOP.Text,Calendar.TodaysDate, txtPrice.Text,txtTracking.Text);
            int noRows = Int32.Parse(value);
            if (noRows > 0)
            {
                lblText.Text = "Data inserted";
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
                try
                {
                SqlConnection sqlCon;
                        sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                        sqlCon.Open();
                        SqlCommand cmd = new SqlCommand("Update Courier1 set Tracking ='" + txtTracking.Text + "', Price= '" + txtPrice.Text + "' where PackageId='" + txtPackageId.Text + "'", sqlCon);

                    int numberOfRecords = cmd.ExecuteNonQuery();

                    if (numberOfRecords > 0)
                    {
                        lblText.Text = "Record Updated ";
                    }
                }
                catch (Exception ex)
                {
                    lblText.Text = "Error updating Data" + ex;
                }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon;
                sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                SqlCommand cmd = new SqlCommand("Select * from Courier1 where PackageId='" + txtPackageId.Text + "' ", sqlCon);
                SqlDataReader rd = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                lblText.Text = "Error display data" + ex;
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Homepage.aspx");
        }

        protected void btnSearch_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx");
        }
    }
}