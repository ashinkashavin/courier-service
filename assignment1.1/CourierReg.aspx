﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourierReg.aspx.cs" Inherits="assignment1._1.CourierReg" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <style>
.btn-black {
    background-color: #ea4c88;
     margin : 10px;
  padding : 15px;
  width : 10%;
   font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
} 
.btn-secondary {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  width : 10%;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        .form-control {
        margin-block-start : 5px;
        padding : 5px;
        border-radius: 5px;
        }
    </style>
</head>
<body style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div><center>
            <h1>YIEN EXPRESS</h1>
            <div class="form-group">
                     <label>CourierCompanyId&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>&nbsp;<asp:TextBox ID="txtCourierCompanyId" class="form-control" runat="server"></asp:TextBox>
                  </div>
                   <div class="form-group">
                     <label>CompanyName&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                       <asp:TextBox ID="txtCompanyName" class="form-control" runat="server"></asp:TextBox>
                  </div>
                   <div class="form-group">
                     <label>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                     &nbsp;<asp:TextBox ID="txtEmail" class="form-control" runat="server"></asp:TextBox>
                  </div>
                  <div class="form-group">
                     <label>Password</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                  </div>
                   <div class="form-group">
                     <label>Confirm Password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>&nbsp;
                       <asp:TextBox ID="txtCONPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                  </div>
                   <asp:Button ID="btnSubmit" runat="server" Text="Register" class="btn btn-black" OnClick="btnSubmit_Click"  />
                   <asp:Button ID="btnLoginP" runat="server" Text="Login" class="btn btn-secondary" OnClick="btnLoginP_Click"   />
</center>
        </div>
    </form>
    <asp:Label ID="lblTxt" runat="server" Text=""></asp:Label>

</body>
</html>
