﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace assignment1._1
{
    public partial class PackageInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon;
                sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                sqlCon.Open();
                SqlCommand cmd = new SqlCommand("Update Courier1 set Tracking ='" + txtTracking.Text + "', Price= '" + txtPrice.Text + "' where PackageId='" + txtPackageId.Text + "'", sqlCon);
                int numberOfRecords = cmd.ExecuteNonQuery();
                if (numberOfRecords > 0)
                {
                    lblText.Text = "Record Updated ";
                }
            }
            catch (Exception ex)
            {
                lblText.Text = "Error updating Data" + ex;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection sqlCon;
                sqlCon = new SqlConnection("Data Source=DESKTOP-MUL7T7K;Initial Catalog=Yien Express;Integrated Security=True");
                sqlCon.Open();
                SqlCommand cmd = new SqlCommand("Select * from Courier1 where PackageId='" + txtPackageId.Text + "' ", sqlCon);
                SqlDataReader rd = cmd.ExecuteReader();

                if (rd.Read())
                {

                    txtCusName.Text = rd[1].ToString();
                    txtAddress.Text = rd[2].ToString();
                    txtConNumber.Text = rd[3].ToString();
                    txtQuantity.Text = rd[4].ToString();
                    txtNOP.Text = rd[5].ToString();
                    txtDate.Text = rd[6].ToString();
                    txtPrice.Text = rd[7].ToString();
                    txtTracking.Text = rd[8].ToString();
                }
                else
                {
                    lblText.Text = "Data not found";
                }
                rd.Close();
            }
            catch (Exception ex)
            {
                lblText.Text = "Error display data" + ex;
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminDashboard.aspx");
        }
    }
}

