﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1._1
{
    public partial class CourierLogin : System.Web.UI.Page
    {
        ServiceReference4.CourierLoginSoapClient obj = new ServiceReference4.CourierLoginSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (obj.authenticateNoClient(txtEmail.Text, txtPassword.Text))
            {
                Response.Redirect("CourierTracking.aspx");
            }

            else
            {
                Response.Write("<script>alert('Invalid username or password')</script>");
            }
        }

        protected void btnRegisterP_Click(object sender, EventArgs e)
        {
            Response.Redirect("CourierReg.aspx");
        }
    }
}