﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;

namespace assignment1._1
{
    public partial class Normal_Register : System.Web.UI.Page
    {
       
        ServiceReference2.loginSoapClient obj = new ServiceReference2.loginSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {

            txtClientId.Text = obj.AutoClientId();

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string value = obj.insertClient(txtClientId.Text, txtClientName.Text, txtAddress.Text, txtEmail.Text, txtPassword.Text, txtCONPassword.Text);
            int norecord = Int32.Parse(value);


            if (txtClientName.Text != "" && txtAddress.Text != "" && txtEmail.Text != "" && txtPassword.Text != "" && txtCONPassword.Text != "")
            {
                if (txtPassword.Text == txtCONPassword.Text)
                {
                    if (norecord > 0)
                    {
                        lblTxt.ForeColor = System.Drawing.Color.Green;
                        lblTxt.Text = "Successfully registered";

                        txtClientName.Text = "";
                        txtAddress.Text = "";
                        txtEmail.Text = "";
                       
                    }
                }
                else
                {
                    lblTxt.ForeColor = System.Drawing.Color.Red;
                    lblTxt.Text = "Password and confirm password should be matched";
                }
            }

            else
            {
                lblTxt.ForeColor = System.Drawing.Color.Red;
                lblTxt.Text = "Fields cannot be blank";
            }


        }

        protected void btnLoginP_Click(object sender, EventArgs e)
        {
            Response.Redirect("Normal Login.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("Search.aspx");
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {

        }
    }
}