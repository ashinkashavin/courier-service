﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourierLogin.aspx.cs" Inherits="assignment1._1.CourierLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<style>
.btn-black {
    background-color: #ea4c88;
     margin : 10px;
  padding : 15px;
  width : 10%;
   font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
} 
.btn-secondary {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  width : 10%;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        .form-control {
        margin-block-start : 5px;
        padding : 5px;
        border-radius: 5px;
        }
    </style>
</head>
<body style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div>
            <center>  <h1>YIEN - EXPRESS</h1></center>
            <center>
         <div class="form-group">
                     <label> Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>&nbsp;
             <asp:TextBox ID="txtEmail" class="form-control" runat="server" Width="251px"></asp:TextBox>
                     <br />
                     <br />
                  </div>
                  <div class="form-group">
                     <label>Password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>&nbsp;
                      <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password" Width="249px"></asp:TextBox>
                      <br />
                      <br />
                      <br />
                  </div>
                   <asp:Button ID="btnLogin" runat="server" Text="Login" class="btn btn-black" OnClick="btnLogin_Click"/>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Button ID="btnRegisterP" runat="server" Text="Register" class="btn btn-secondary" OnClick="btnRegisterP_Click"/>
      </center> 
        </div>
    </form>
</body>
</html>
