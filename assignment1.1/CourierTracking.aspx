﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourierTracking.aspx.cs" Inherits="assignment1._1.CourierTracking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<style>
.btn-black {
    background-color: #ea4c88;
     margin : 10px;
  padding : 15px;
  width : 10%;
   font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
} 
.btn-secondary {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        .form-control {
        margin-block-start : 5px;
        padding : 5px;
        border-radius: 5px;
        }
         .auto-style1 {
             height: 60px;
             width: 356px;
         }
         .auto-style2 {
             height: 85px;
         }
         .auto-style3 {
             margin-top: 0px;
         }
         .auto-style4 {
             height: 85px;
             width: 356px;
         }
         .auto-style5 {
             width: 356px;
         }
    </style>
</head>
<body style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div>
            <center>
            <h1>YIEN EXPRESS</h1>
             <table style="height: 425px; width: 461px">
        <tr>
            <td>
                Package Id
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPackageId" runat="server" Width="239px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Customer Name
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtCusName" runat="server" Width="236px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">
                Address
            </td>
            <td class="auto-style4">
                <asp:TextBox ID="txtAddress" runat="server" Height="61px" TextMode="MultiLine" Width="273px" CssClass="auto-style3"></asp:TextBox>
            </td>
        </tr>
           <tr>
            <td>
                Contact Number
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtConNumber" runat="server" style="margin-left: 6px" Width="219px"></asp:TextBox>
            </td>
        </tr>
           <tr>
            <td>
                Quantity
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtQuantity" runat="server" style="margin-left: 0px" Width="217px"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td>
                Nature of Package
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtNOP" runat="server" style="margin-right: 0px" Width="221px"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td>
                Date
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtDate" runat="server" Width="222px"></asp:TextBox>
                
            </td>
        </tr>
          <tr>
            <td>
                Price
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPrice" runat="server" style="margin-left: 0px" Width="217px"></asp:TextBox>
            </td>
        </tr>
                  <tr>
            <td>
               Tracking
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtTracking" runat="server" style="margin-left: 0px" Width="222px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            
            <td><asp:Button ID="btnUpdate" runat="server" class="btn btn-secondary" Text="Update" OnClick="btnUpdate_Click" Width="134px" /></td>
              <td class="auto-style5"><asp:Button ID="btnSearch" runat="server" class="btn btn-secondary" Text="Search" OnClick="btnSearch_Click" Width="189px" />
                  <td><asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-secondary" OnClick="btnBack_Click" /></td>
                  <br />
            </td>
        </tr>
    </table>
                <br />
                <br />
        <asp:Label ID="lblText" runat="server" Text="Label"></asp:Label>
       </center> 
        </div>
    </form>
</body>
</html>
