﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="assignment1._1.Search" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
.btn-black {
    background-color: #ea4c88;
     margin : 10px;
  padding : 15px;
  width : 10%;
   font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
} 
.btn-secondary {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        .form-control {
        margin-block-start : 5px;
        padding : 5px;
        border-radius: 5px;
        }
       
        .auto-style1 {
            width: 342px;
        }
       
    </style>
</head>
<body style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div>
            <center>
            <table>
        <tr><td><h3>Enter Package Id</h3></td>
        <td class="auto-style1">
            <asp:TextBox ID="txtPackageId" runat="server"></asp:TextBox></td>
            <td>

                <asp:Button ID="btnSearch" runat="server" class="btn-black" Text="Search" OnClick="btnSearch_Click" Height="57px" Width="125px" /></td>
</tr>
                  <tr>
            <td>
                Customer Name
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtCusName" runat="server"></asp:TextBox>
            </td>
                            <td><asp:Button ID="btnBack" runat="server" Text="Back" class="btn btn-secondary" OnClick="btnBack_Click" /></td> 
        </tr>
        <tr>
            <td>
                Address
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtAddress" runat="server" Height="41px" TextMode="MultiLine" Width="276px"></asp:TextBox>
            </td>
        </tr>
           <tr>
            <td>
                Contact Number
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtConNumber" runat="server"></asp:TextBox>
            </td>
        </tr>
           <tr>
            <td>
                Quantity
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td>
                Nature_of_Package
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtNOP" runat="server"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td>
                Date
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                
            </td>
        </tr>
          <tr>
            <td>
                Price
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
            </td>
        </tr>
                  <tr>
            <td>
               Tracking
            </td>
            <td class="auto-style1">
                <asp:TextBox ID="txtTracking" runat="server"></asp:TextBox>
            </td>
        </tr>
        
    </table>
                    <asp:Label ID="lblText" runat="server" Text="Label"></asp:Label>
    </center>   

        </div>
    </form>
</body>
</html>
