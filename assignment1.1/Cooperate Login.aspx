﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cooperate Login.aspx.cs" Inherits="assignment1._1.Cooperate_Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
.btn-black {
    background-color: #ea4c88;
     margin : 10px;
  padding : 15px;
  width : 10%;
   font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
} 
.btn-secondary {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  width : 10%;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        .form-control {
        margin-block-start : 5px;
        padding : 5px;
        border-radius: 5px;
        }
    </style>
</head>
<body style="background-color:#2c3338;">
    <form id="form1" runat="server">
      <center>  <h1>YIEN - EXPRESS</h1></center>
        <div>
            <center>
            <div class="form-group">
                     <label>Cooperate Email</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <asp:TextBox ID="txtEmail" class="form-control" runat="server"></asp:TextBox>
                  </div>
                <br />
                <br />
                <br />
                  <div class="form-group">
                     <label>Password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                      &nbsp;<asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                  </div>
                <p></p>
                <p></p>
                   <asp:Button ID="btnLogin" runat="server" Text="Login" class="btn btn-black" OnClick="btnLogin_Click"   />
                   <asp:Button ID="btnRegisterP" runat="server" Text="Register" class="btn btn-secondary" OnClick="btnRegisterP_Click"   />
                
</center>       
        </div>
    </form>
</body>
</html>