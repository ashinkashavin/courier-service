﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminDashboard.aspx.cs" Inherits="assignment1._1.AdminDashboard" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
.btnAdmin {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  width : 10%;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        
    </style>
</head>
<body  style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div><center>
            <h1>Welcome Admin</h1>
            <p>&nbsp;</p>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Button ID="btnAddprice" runat="server" style="width:250px;" class="btnAdmin" Text="Add Price" OnClick="btnAddprice_Click"/>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnUpdtracking"  runat="server" style="width:250px;" class="btnAdmin" Text="Update tracking " OnClick="btnUpdtracking_Click" /> 
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnSearch" runat="server" style="width:250px;" class="btnAdmin" Text="Search" OnClick="btnSearch_Click" />
                            <br />
                            <br />
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnLogout" runat="server" class="btnAdmin" Text="Logout" OnClick="btnLogout_Click"  />
                        </div>
                    </div>
                </div>
        </div></center>
    </form>
</body>
</html>
