﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;

namespace assignment1._1
{
    public partial class Cooperate_Register : System.Web.UI.Page
    {
        ServiceReference3.loginCooprateSoapClient obj = new ServiceReference3.loginCooprateSoapClient();
        
        protected void Page_Load(object sender, EventArgs e)
        {
           
           txtUserId.Text=obj.AutoCoopClientId();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
                string value = obj.insertClient(txtUserId.Text, txtName.Text, txtAddress.Text, txtEmail.Text, dlPackage.SelectedItem.ToString(), txtPassword.Text, txtCPassword.Text);
                int norecord = Int32.Parse(value);


                if (txtName.Text != "" && txtAddress.Text != "" && txtEmail.Text != "" && txtPassword.Text != "" && txtCPassword.Text != "")
                {
                    if (txtPassword.Text == txtCPassword.Text)
                    {
                        if (norecord > 0)
                        {
                            lblTxt.ForeColor = System.Drawing.Color.Green;
                            lblTxt.Text = "Successfully registered";

                            txtName.Text = "";
                            txtAddress.Text = "";
                            txtEmail.Text = "";
                           
                        }
                    }
                    else
                    {
                        lblTxt.ForeColor = System.Drawing.Color.Red;
                        lblTxt.Text = "Password and confirm password should be matched";
                    }
                }

                else
                {
                    lblTxt.ForeColor = System.Drawing.Color.Red;
                    lblTxt.Text = "Fields cannot be blank";
                }
            }

            protected void btnLoginP_Click(object sender, EventArgs e)
            {
                Response.Redirect("Cooperate Login.aspx");
            }

    }
}
