﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Homepage.aspx.cs" Inherits="assignment1._1.Homepage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style>
          .btnHomepage {
  background-color: #ea4c88;
  margin : 10px;
  padding : 15px;
  font-weight: 700;
    text-transform: uppercase;
    border-radius: 5px;
}
        body {
        min-height: 150vh;
        }
        
    </style>
</head>
<body   style="background-color:#2c3338;">
    <form id="form1" runat="server">
        <div>
            <center><h1>YIEN EXPRESS</h1>
                <p>&nbsp;</p></center>
 <center>          
<tr>
    <td>
            <asp:Button ID="btnNormalLogin" runat="server" Text="Normal Customer Login" Class="btnHomepage" OnClick="btnNormalLogin_Click" Width="286px"/>
            <br />
            </td>

</tr>
     <tr>
            <td>
                <asp:Button ID="btnCooperate" runat="server" Text="Cooperate Customer Login" Class="btnHomepage" OnClick="btnCooperate_Click" Width="309px"/>
            </td>

     </tr>
     <tr>
            <td>
        <br />
        <asp:Button ID="btnAdmin" runat="server" Text="Admin Login" Class="btnHomepage" OnClick="btnAdmin_Click"/>
            <br />
            </td> 
     </tr>
     <tr>
            <td>
        <br />
        <asp:Button ID="btnCourier" runat="server" Text="Courier Login" Class="btnHomepage" OnClick="btnCourier_Click"/>
            <br />
            </td> 
     </tr>
     <tr>
    <td>
        <asp:Button ID="btnpackagedetails" runat="server" Text="Package Details" Class="btnHomepage" OnClick="btnpackagedetails_Click"/>
            </td>

        </tr>
 </center> 
        </div>
    </form>
</body>
</html>
