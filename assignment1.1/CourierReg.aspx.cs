﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1._1
{
    public partial class CourierReg : System.Web.UI.Page
    {
        ServiceReference4.CourierLoginSoapClient obj = new ServiceReference4.CourierLoginSoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtCourierCompanyId.Text = obj.AutoCourierClientId();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string value = obj.insertClient(txtCourierCompanyId.Text, txtCompanyName.Text, txtEmail.Text, txtPassword.Text, txtCONPassword.Text);
            int norecord = Int32.Parse(value);


            if (txtCompanyName.Text != "" && txtEmail.Text != "" && txtPassword.Text != "" && txtCONPassword.Text != "")
            {
                if (txtPassword.Text == txtCONPassword.Text)
                {
                    if (norecord > 0)
                    {
                        lblTxt.ForeColor = System.Drawing.Color.Green;
                        lblTxt.Text = "Successfully registered";

                        txtCompanyName.Text = "";
                        txtEmail.Text = "";

                    }
                }
                else
                {
                    lblTxt.ForeColor = System.Drawing.Color.Red;
                    lblTxt.Text = "Password and confirm password should be matched";
                }
            }

            else
            {
                lblTxt.ForeColor = System.Drawing.Color.Red;
                lblTxt.Text = "Fields cannot be blank";
            }


        }

        protected void btnLoginP_Click(object sender, EventArgs e)
        {
            Response.Redirect("CourierLogin.aspx");
        }
    }
}