﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment1._1
{
    public partial class Homepage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnNormalLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("Normal Login.aspx");
        }

        protected void btnCooperate_Click(object sender, EventArgs e)
        {
            Response.Redirect("Cooperate Login.aspx");
        }

        protected void btnAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminLogin.aspx");
        }

        protected void btnpackagedetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("Package details.aspx");
        }

        protected void btnCourier_Click(object sender, EventArgs e)
        {
            Response.Redirect("CourierLogin.aspx");
        }
    }
}